﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.Repository;
using XPos.ViewModel;

namespace XPos.Controllers
{
    public class VariantController : Controller
    {
        // GET: Variant
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            List<VariantViewModel> ListVariant = VariantRepo.GetAll();
            return PartialView("_List", ListVariant);
        }

        public ActionResult ListByCategory(long id = 0)
        {
            return PartialView("_ListByCategory", VariantRepo.ByCategory(id));
        }
        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "id", "Name");
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "id", "Name");
            return PartialView("_Edit", VariantRepo.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {

            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "id", "Name");
            return PartialView("_Delete", VariantRepo.GetById(id));
        }

        [HttpPost]
        public ActionResult Delete(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity,
            }, JsonRequestBehavior.AllowGet);
        }
    }
}