﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpos.Model;
using XPos.ViewModel;

namespace XPos.Repository
{
    public class VariantRepo
    {
        //Get All
        public static List<VariantViewModel> GetAll()
        {
            List<VariantViewModel> result = new
                List<VariantViewModel>();

            using (var db = new XPosContext())
                //    {
                //        result = (from var in db.Variants
                //                  select new VariantViewModel
                //                  {
                //                      id = var.id,
                //                      CategoryId = var.Category.id,
                //                      Initial = var.Initial,
                //                      Name = var.Name,
                //                      Active = var.Active
                //                  }).ToList();
                //    }
                return ByCategory(-1);
        }

        public static List<VariantViewModel> ByCategory(long id)
        {
            // id => Category Id
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(v => v.CategoryId == (id == -1 ? v.CategoryId : id))
                    .Select(c => new VariantViewModel
                    {
                        id = c.id,
                        CategoryId = c.CategoryId,
                        CategoryName = c.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active
                    }).ToList();
            }
            return result;
        }

        //Get by Id
        public static VariantViewModel GetById(int id)
        {
            VariantViewModel result = new VariantViewModel();

            using (var db = new XPosContext())
            {
                result = (from var in db.Variants
                          where var.id == id
                          select new VariantViewModel
                          {
                              id = var.id,
                              CategoryId = var.Category.id,
                              CategoryName = var.Category.Name,
                              Initial = var.Initial,
                              Name = var.Name,
                              Active = var.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new VariantViewModel();
        }

        //Update
        public static ResponseResult Update(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        Variant variant = new Variant();

                        variant.Initial = entity.Initial;
                        variant.CategoryId = entity.CategoryId;
                        //variant.Category.Name = entity.CategoryName;
                        variant.Name = entity.Name;
                        variant.Active = entity.Active;

                        variant.CreatedBy = "Arum Dian";
                        variant.CreatedDate = DateTime.Now;

                        db.Variants.Add(variant);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region Edit
                    else
                    {
                        Variant variant = db.Variants.Where(o =>
                        o.id == entity.id).FirstOrDefault();

                        if (variant != null)
                        {
                            variant.Initial = entity.Initial;
                            variant.CategoryId = entity.CategoryId;
                            //variant.Category.Name = entity.CategoryName;
                            variant.Name = entity.Name;
                            variant.Active = entity.Active;

                            variant.ModifiedBy = "Arum Dian";
                            variant.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Variant is not found";
                        }
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Variant variant = db.Variants.Where(o => o.id == entity.id).FirstOrDefault();
                    if (variant != null)
                    {
                        result.Entity = entity;
                        db.Variants.Remove(variant);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Variant is not found";
                    }

                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                throw;
            }
            return result;
        }
    }
}
