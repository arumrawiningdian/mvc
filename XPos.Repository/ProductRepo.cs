﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpos.Model;
using XPos.ViewModel;

namespace XPos.Repository
{
    public class ProductRepo
    {
        //Get All
        public static List<ProductViewModel> GetAll()
        {
            List<ProductViewModel> result = new
                List<ProductViewModel>();

            using (var db = new XPosContext())
                
                return ByVariant(-1);
        }

        public static List<ProductViewModel> ByVariant(long id)
        {
            // id => Variant Id
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .Where(p => p.VariantId == (id == -1 ? p.VariantId : id))
                    .Select(v => new ProductViewModel
                    {
                        Id = v.Id,
                        VariantId = v.VariantId,
                        VariantName = v.Variant.Name,
                        Initial = v.Initial,
                        Name = v.Name,
                        Description = v.Description,
                        Price = v.Price,
                        Active = v.Active
                    }).ToList();
            }
            return result;
        }

        //Get by Id
        public static ProductViewModel GetById(int id)
        {
            ProductViewModel result = new ProductViewModel();

            using (var db = new XPosContext())
            {
                result = (from pro in db.Products
                          where pro.Id == id
                          select new ProductViewModel
                          {
                              Id = pro.Id,
                              VariantId = pro.Variant.id,
                              VariantName = pro.Variant.Name,
                              Initial = pro.Initial,
                              Name = pro.Name,
                              Description = pro.Description,
                              Price = pro.Price,
                              Active = pro.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new ProductViewModel();
        }

        //Update
        public static ResponseResult Update(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create New / Insert 
                    if (entity.Id == 0)
                    {
                        Product product = new Product();

                        product.Initial = entity.Initial;
                        product.VariantId = entity.VariantId;
                        //product.Variant.Name = entity.VariantName;
                        product.Name = entity.Name;
                        product.Description = entity.Description;
                        product.Price = entity.Price;
                        product.Active = entity.Active;

                        product.CreatedBy = "Arum Dian";
                        product.CreatedDate = DateTime.Now;

                        db.Products.Add(product);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region Edit
                    else
                    {
                        Product product = db.Products.Where(o =>
                        o.Id == entity.Id).FirstOrDefault();

                        if (product != null)
                        {
                            product.Initial = entity.Initial;
                            product.VariantId = entity.VariantId;
                            //product.Variant.Name = entity.VariantName;
                            product.Name = entity.Name;
                            product.Description = entity.Description;
                            product.Price = entity.Price;
                            product.Active = entity.Active;

                            product.ModifiedBy = "Arum Dian";
                            product.ModifyDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Product is not found";
                        }
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Product product = db.Products.Where(o => o.Id == entity.Id).FirstOrDefault();
                    if (product != null)
                    {
                        result.Entity = entity;
                        db.Products.Remove(product);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Product is not found";
                    }

                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                throw;
            }
            return result;
        }
    }
}
