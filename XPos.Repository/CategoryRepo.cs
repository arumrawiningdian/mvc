﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpos.Model;
using XPos.ViewModel;

namespace XPos.Repository
{
    public class CategoryRepo
    {
        //Get All
        public static List<CategoryViewModel> GetAll()
        {
            List<CategoryViewModel> result = new
                List<CategoryViewModel>();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Categories
                          select new CategoryViewModel
                          {
                              id = cat.id,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).ToList();
            }
            return result;
        }

        //Get by Id
        public static CategoryViewModel GetById(int id)
        {
            CategoryViewModel result = new CategoryViewModel();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Categories
                          where cat.id == id
                          select new CategoryViewModel
                          {
                              id = cat.id,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new CategoryViewModel();
        }

        //Update
        public static ResponseResult Update (CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create New / Insert 
                    if (entity.id == 0)
                    {
                        Category category = new Category();

                        category.Initial = entity.Initial;
                        category.Name = entity.Name;
                        category.Active = entity.Active;

                        category.CreatedBy = "Arum Dian";
                        category.CreatedDate = DateTime.Now;

                        db.Categories.Add(category);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region Edit
                    else
                    {
                        Category category = db.Categories.Where(o => 
                        o.id == entity.id).FirstOrDefault();

                        if (category != null)
                        {
                            category.Initial = entity.Initial;
                            category.Name = entity.Name;
                            category.Active = entity.Active;

                            category.ModifiedBy = "Arum Dian";
                            category.ModifiedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category is not found";
                        }
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                
            }
            return result;
        }

        //Delete
        public static ResponseResult Delete(CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Category category = db.Categories.Where(o => o.id == entity.id).FirstOrDefault();
                    if (category != null)
                    {
                        result.Entity = entity;
                        db.Categories.Remove(category);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category is not found";
                    }

                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                throw;
            }
            return result;
        }
    }
}
