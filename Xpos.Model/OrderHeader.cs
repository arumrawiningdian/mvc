namespace Xpos.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderHeader")]
    public partial class OrderHeader
    {
        public long Id { get; set; }

        [Required]
        [StringLength(15)]
        public string Reference { get; set; }

        public decimal Amount { get; set; }

        public bool Active { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        [StringLength(10)]
        public string ModifiedDate { get; set; }

        public virtual OrderDetail OrderDetail { get; set; }
    }
}
